import 'package:flutter/material.dart';

import 'Application.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(Application());
}
