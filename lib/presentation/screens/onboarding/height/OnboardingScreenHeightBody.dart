import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:losses/constants/Dimensions.dart';
import 'package:losses/extensions/theme/Styles.dart';
import 'package:losses/presentation/widgets/ScrollPicker.dart';

class OnboardingScreenHeightBody extends StatelessWidget{
  const OnboardingScreenHeightBody({Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    List heightList = List.generate(121, (index) => 100 + index);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          AppLocalizations.of(context)!.onboardingHeightTitle,
          style: Styles.of(context).onboardingTitleTextStyle,
        ),
        const SizedBox(
          height: Dimensions.onboardingMarginTitle,
        ),
        SizedBox(
            height: Dimensions.onboardingSizePicker,
            child: ScrollPicker(
                items: heightList, initialItem: 175, onChanged: (item) {})),
        const SizedBox(
          height: Dimensions.onboardingMarginPicker,
        ),
      ],
    );
  }

}