import 'package:flutter/material.dart';
import 'package:losses/constants/Dimensions.dart';
import 'package:losses/extensions/theme/Styles.dart';
import 'package:losses/presentation/widgets/ScrollPicker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OnboardingScreenWeightBody extends StatefulWidget {
  const OnboardingScreenWeightBody({Key? key}) : super(key: key);

  @override
  _OnboardingScreenWeightBodyState createState() => _OnboardingScreenWeightBodyState();
}

class _OnboardingScreenWeightBodyState extends State<OnboardingScreenWeightBody> {
  late final List<int> weightKgList;
  late final List<int> weightPartKgList;

  @override
  void initState() {
    super.initState();
    weightKgList = List.generate(181, (index) => 20 + index);
    weightPartKgList = List.generate(10, (index) => index);
  }
  @override
  Widget build(BuildContext context) {
 
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          AppLocalizations.of(context)!.onboardingWeightTitle,
          style: Styles.of(context).onboardingTitleTextStyle,
        ),
        const SizedBox(
          height: Dimensions.onboardingMarginTitle,
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: Dimensions.onboardingSizePicker,
              width: MediaQuery.of(context).size.width / 2 -
                  (Dimensions.onboardingMarginDot * 2 +
                      Dimensions.onboardingDotSize),
              child: ScrollPicker(
                  items: weightKgList, initialItem: 75, onChanged: (item) {}),
            ),
            Container(
              height: Dimensions.onboardingDotSize,
              width: Dimensions.onboardingDotSize,
              decoration: BoxDecoration(
                  color: Styles.of(context).primary, shape: BoxShape.circle),
            ),
            SizedBox(
              height: Dimensions.onboardingSizePicker,
              width: MediaQuery.of(context).size.width / 2 -
                  (Dimensions.onboardingMarginDot * 2 +
                      Dimensions.onboardingDotSize),
              child: ScrollPicker(
                  items: weightPartKgList, initialItem: 5, onChanged: (item) {}),
            ),
          ],
        ),
        const SizedBox(
          height: Dimensions.onboardingMarginPicker,
        ),
      ],
    );
  }
}
