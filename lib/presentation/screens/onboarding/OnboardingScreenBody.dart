import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:losses/constants/Dimensions.dart';
import 'package:losses/presentation/screens/onboarding/age/OnboardingScreenAgeBody.dart';
import 'package:losses/presentation/screens/onboarding/weight/OnboardingScreenWeightBody.dart';
import 'package:losses/presentation/widgets/Button.dart';
import 'package:losses/presentation/widgets/PageViewIndicator.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'desiredweight/OnboardingScreenDesiredWeightBody.dart';
import 'height/OnboardingScreenHeightBody.dart';

class OnboardingScreenBody extends StatefulWidget {
  final Function(int) onPageChanged;
  final PageController controller;

  const OnboardingScreenBody(
      {Key? key, required this.onPageChanged, required this.controller})
      : super(key: key);

  @override
  _OnboardingScreenBodyState createState() => _OnboardingScreenBodyState();
}

class _OnboardingScreenBodyState extends State<OnboardingScreenBody> {
  late final List<Widget> listScreens;
  int _activePage = 0;

  @override
  void initState() {
    super.initState();
    listScreens = [
      const OnboardingScreenAgeBody(),
      const OnboardingScreenHeightBody(),
      const OnboardingScreenWeightBody(),
      const OnboardingScreenDesiredWeightBody(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        alignment: AlignmentDirectional.center,
        fit: StackFit.expand,
        children: [
          PageView(
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            controller: widget.controller,
            children: <Widget>[
              Center(child: listScreens[0]),
              Center(child: listScreens[1]),
              Center(child: listScreens[2]),
              Center(child: listScreens[3]),
            ],
            onPageChanged: (position) {
              widget.onPageChanged.call(position);
              _activePage = position;
            },
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  PageViewIndicator(
                    countPages: listScreens.length,
                    activePage: _activePage,
                  ),
                  SizedBox(
                    height: 42,
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 24, right: 24, bottom: 36),
                    child: Button(
                        text: _activePage != listScreens.length - 1
                            ? AppLocalizations.of(context)!.onboardingNextBtn
                            : AppLocalizations.of(context)!.onboardingDoneBtn,
                        onPressed: () {
                          widget.controller.nextPage(
                              duration: Duration(
                                  milliseconds:
                                      Dimensions.onboardingDurationChangePage),
                              curve: Curves.easeIn);
                        }),
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
