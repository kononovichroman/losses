import 'package:flutter/material.dart';
import 'package:losses/constants/Dimensions.dart';
import 'package:losses/extensions/theme/Styles.dart';
import 'package:losses/presentation/widgets/ScrollPicker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class OnboardingScreenAgeBody extends StatelessWidget{
  const OnboardingScreenAgeBody({Key? key}): super(key: key);
  @override
  Widget build(BuildContext context) {
    List ageList = List.generate(70, (index) => 10 + index);
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          AppLocalizations.of(context)!.onboardingAgeTitle,
          style: Styles.of(context).onboardingTitleTextStyle,
        ),
        const SizedBox(
          height: Dimensions.onboardingMarginTitle,
        ),
        SizedBox(
            height: Dimensions.onboardingSizePicker,
            child: ScrollPicker(
                items: ageList, initialItem: 27, onChanged: (item) {})),
        const SizedBox(
          height: Dimensions.onboardingMarginPicker,
        ),
      ],
    );
  }

}