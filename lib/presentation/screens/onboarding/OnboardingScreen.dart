import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:losses/constants/Dimensions.dart';
import 'package:losses/extensions/adaptive/AdaptiveScaffold.dart';
import 'package:losses/extensions/theme/Styles.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'OnboardingScreenBody.dart';

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  List<String> _arrayAppBarTitles = [];
  int _currentPage = 0;
  late final PageController _controller;

  @override
  void initState() {
    super.initState();
    _controller = PageController(initialPage: 0);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _arrayAppBarTitles = [
      AppLocalizations.of(context)!.onboardingAgeAppbar,
      AppLocalizations.of(context)!.onboardingHeightAppbar,
      AppLocalizations.of(context)!.onboardingWeightAppbar,
      AppLocalizations.of(context)!.onboardingDesiredWeightAppbar,
    ];
    return AdaptiveScaffold(
        title: Text(AppLocalizations.of(context)!.onboardingAgeAppbar),
        cupertinoNavigationBar: CupertinoNavigationBar(
          padding: EdgeInsetsDirectional.zero,
          backgroundColor: Colors.transparent,
          middle: Text(
            _arrayAppBarTitles[_currentPage],
            style: TextStyle(color: Styles.of(context).onSurface),
          ),
          border: null,
          leading: CupertinoNavigationBarBackButton(
            color: Styles.of(context).primary,
            onPressed: () {
              if (_currentPage != 0) {
                _controller.previousPage(
                    duration: Duration(
                        milliseconds: Dimensions.onboardingDurationChangePage),
                    curve: Curves.easeIn);
              } else {
                Navigator.pop(context);
              }
            },
          ),
        ),
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.transparent,
          title: Text(
            _arrayAppBarTitles[_currentPage],
            style: TextStyle(color: Styles.of(context).onSurface),
          ),
          shadowColor: Colors.transparent,
          leading: BackButton(
            color: Styles.of(context).primary,
            onPressed: () {
              if (_currentPage != 0) {
                _controller.previousPage(
                    duration: Duration(
                        milliseconds: Dimensions.onboardingDurationChangePage),
                    curve: Curves.easeIn);
              } else {
                Navigator.pop(context);
              }
            },
          ),
        ),
        backgroundColor: Styles.of(context).surface,
        body: OnboardingScreenBody(
          controller: _controller,
          onPageChanged: (page) {
            setState(() {
              _currentPage = page;
            });
          },
        ));
  }
}
