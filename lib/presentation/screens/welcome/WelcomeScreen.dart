import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:losses/extensions/adaptive/AdaptiveScaffold.dart';
import 'package:losses/extensions/theme/Styles.dart';

import 'WelcomeScreenBody.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AdaptiveScaffold(
      body: SafeArea(child: WelcomeScreenBody()),
      backgroundColor: Styles.of(context).surface,
    );
  }
}
