import 'package:flutter/material.dart';
import 'package:losses/extensions/theme/Styles.dart';
import 'package:losses/presentation/screens/onboarding/OnboardingScreen.dart';
import 'package:losses/presentation/widgets/Button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class WelcomeScreenBody extends StatefulWidget {
  const WelcomeScreenBody({Key? key}) : super(key: key);

  @override
  _WelcomeScreenBodyState createState() => _WelcomeScreenBodyState();
}

class _WelcomeScreenBodyState extends State<WelcomeScreenBody> {
  @override
  void initState() {
    startAnimation();
    super.initState();
  }

  Future<void> startAnimation() async {
    await Future.delayed(Duration(milliseconds: 700), () {
      setState(() {
        opacitySecondLine = 1.0;
      });
    });
  }

  double opacitySecondLine = 0.0;
  double opacityThirdLine = 0.0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: AlignmentDirectional.centerStart,
      fit: StackFit.passthrough,
      children: [
        Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 56),
              child: Image(image: AssetImage('assets/images/welcome_icon.png')),
            )),
        Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 32, bottom: 8, right: 32),
                child: RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                          text: AppLocalizations.of(context)!
                              .welcomeScreenTitleFirstLineStart),
                      TextSpan(
                          text: AppLocalizations.of(context)!
                              .welcomeScreenTitleFirstLineEnd,
                          style: TextStyle(
                              color: Styles.of(context).primary,
                              fontWeight: FontWeight.bold)),
                    ],
                    style: TextStyle(
                      color: Styles.of(context).onSurface,
                      fontSize: 32,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32, bottom: 8, right: 32),
                child: AnimatedOpacity(
                  onEnd: () {
                    setState(() {
                      opacityThirdLine = 1.0;
                    });
                  },
                  duration: Duration(milliseconds: 1400),
                  opacity: opacitySecondLine,
                  child: Text(
                    AppLocalizations.of(context)!.welcomeScreenTitleSecondLine,
                    style: TextStyle(
                        color: Styles.of(context).onSurface,
                        fontSize: 32,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 32),
                child: AnimatedOpacity(
                  duration: Duration(milliseconds: 500),
                  opacity: opacityThirdLine,
                  child: Text(
                    AppLocalizations.of(context)!.welcomeScreenTitleThirdLine,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        color: Styles.of(context).onSurface,
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(left: 32, right: 32, bottom: 144),
            child: Text(
              AppLocalizations.of(context)!.welcomeScreenDescription,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Styles.of(context).onSurfaceMedium,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ),
        ),
        Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(left: 24, right: 24, bottom: 36),
              child: Button(
                  text: AppLocalizations.of(context)!.startButton,
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                      fullscreenDialog: true,
                      builder: (context) => OnboardingScreen(),));
                  }),
            )),
      ],
    );
  }
}
