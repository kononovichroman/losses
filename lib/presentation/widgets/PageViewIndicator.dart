import 'package:flutter/material.dart';
import 'package:losses/extensions/theme/Styles.dart';

const double _kSizeIndicator = 20.0;
const double _kPaddingBetweenItems = 8.0;
const double _kBorderSize = 4.0;

class PageViewIndicator extends StatelessWidget {
  final int countPages;
  final int activePage;

  const PageViewIndicator({Key? key, required this.countPages, required this.activePage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<int>.generate(countPages, (index) => index)
          .map((page) => Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: _kPaddingBetweenItems),
                child: Container(
                  height: _kSizeIndicator,
                  width: _kSizeIndicator,
                  decoration: BoxDecoration(
                      color: page == activePage ? Colors.transparent : page<activePage ? Styles.of(context).primary: Styles.of(context).primary40,
                      shape: BoxShape.circle,
                      border: page == activePage ? Border.all(
                          color: Styles.of(context).primary,
                          width: _kBorderSize) : null),
                ),
              ))
          .toList(),
    );
  }
}
