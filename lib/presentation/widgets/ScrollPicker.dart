import 'package:flutter/material.dart';
import 'package:losses/extensions/theme/Styles.dart';

class ScrollPicker<T> extends StatefulWidget {
  ScrollPicker({
    Key? key,
    required this.items,
    required this.initialItem,
    required this.onChanged,
  }) : super(key: key);

  // Events
  final ValueChanged<T> onChanged;

  // Variables
  final List<T> items;
  final T initialItem;

  @override
  _ScrollPickerState createState() => _ScrollPickerState<T>(initialItem);
}

class _ScrollPickerState<T> extends State<ScrollPicker<T>> {
  _ScrollPickerState(this.selectedValue);

  // Constants
  static const double itemHeight = 43.0;
  static const double itemWidth = 72.0;
  static const double defFontSize = 20.0;
  static const double selectedFontSize = 24.0;
  static const double borderRadius = 16.0;
  static const double gradientOpacityStart = 0.0;
  static const double gradientOpacityEnd = 0.7;

  // Variables
  late double widgetHeight;

  T selectedValue;

  late ScrollController scrollController;

  @override
  void initState() {
    super.initState();
    int initialItem = widget.items.indexOf(selectedValue);
    scrollController = FixedExtentScrollController(initialItem: initialItem);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle defaultStyle =
        TextStyle(color: Styles.of(context).primary, fontSize: defFontSize);
    TextStyle selectedStyle = TextStyle(
        color: Styles.of(context).surface, fontSize: selectedFontSize);

    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        widgetHeight = constraints.maxHeight;
        return Stack(
          children: <Widget>[
            Center(
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(borderRadius),
                    color: Styles.of(context).primary),
                height: itemHeight,
                width: itemWidth,
              ),
            ),
            GestureDetector(
              onTapUp: _itemTapped,
              child: ListWheelScrollView.useDelegate(
                useMagnifier: true,
                childDelegate: ListWheelChildBuilderDelegate(
                    builder: (BuildContext context, int index) {
                  if (index < 0 || index > widget.items.length - 1) {
                    return null;
                  }

                  var value = widget.items[index];

                  final TextStyle? itemStyle =
                      (value == selectedValue) ? selectedStyle : defaultStyle;
                  return Center(
                    child: Text('$value', style: itemStyle),
                  );
                }),
                controller: scrollController,
                itemExtent: itemHeight,
                onSelectedItemChanged: _onSelectedItemChanged,
                physics: FixedExtentScrollPhysics(),
              ),
            ),
            IgnorePointer(
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.topCenter,
                    height: (widgetHeight - itemHeight) / 2,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Styles.of(context).surface.withOpacity(gradientOpacityEnd),
                              Styles.of(context).surface.withOpacity(gradientOpacityStart),
                        ])),
                  ),
                  SizedBox(
                    height: itemHeight,
                  ),
                  Container(
                    alignment: Alignment.bottomCenter,
                    height: (widgetHeight - itemHeight) / 2,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                              Styles.of(context).surface.withOpacity(gradientOpacityStart),
                              Styles.of(context).surface.withOpacity(gradientOpacityEnd),
                        ])),
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  void _itemTapped(TapUpDetails details) {
    Offset position = details.localPosition;
    double center = widgetHeight / 2;
    double changeBy = position.dy - center;
    double newPosition = scrollController.offset + changeBy;

    // animate to and center on the selected item
    scrollController.animateTo(newPosition,
        duration: Duration(milliseconds: 500), curve: Curves.easeInOut);
  }

  void _onSelectedItemChanged(int index) {
    T newValue = widget.items[index];
    if (newValue != selectedValue) {
      setState(() {
        selectedValue = newValue;
      });
      widget.onChanged(newValue);
    }
  }
}
