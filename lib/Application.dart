import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:losses/extensions/adaptive/AdaptiveApp.dart';
import 'package:losses/extensions/adaptive/AdaptiveRoot.dart';
import 'package:losses/presentation/screens/welcome/WelcomeScreen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AdaptiveRoot(
        adaptiveState: AdaptiveRoot.getStateByPlatform(),
        child: AdaptiveApp(
          localizationsDelegates: AppLocalizations.localizationsDelegates,
          supportedLocales: AppLocalizations.supportedLocales,
          home: WelcomeScreen(),
        ));
  }
}
