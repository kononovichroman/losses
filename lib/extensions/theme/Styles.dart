import 'package:flutter/material.dart';

class Styles {
  final Color primary;
  final Color primary60;
  final Color primary40;
  final Color surface;
  final Color onSurface;
  final Color onSurfaceHigh;
  final Color onSurfaceMedium;
  final Color onSurfaceDisabled;
  final Color onSurfaceElevation1dp;
  final Color onSurfaceElevation3dp;
  final Color redColor;
  final Color orangeColor;
  final Color greenColor;
  final Color blueColor;
  final TextStyle onboardingTitleTextStyle;

  const Styles({
    required this.primary,
    required this.primary60,
    required this.primary40,
    required this.surface,
    required this.onSurface,
    required this.onSurfaceHigh,
    required this.onSurfaceMedium,
    required this.onSurfaceDisabled,
    required this.onSurfaceElevation1dp,
    required this.onSurfaceElevation3dp,
    required this.redColor,
    required this.orangeColor,
    required this.greenColor,
    required this.blueColor,
    required this.onboardingTitleTextStyle,
  });

  static Styles of(BuildContext context) {
    final isDarkTheme =
        MediaQuery.of(context).platformBrightness == Brightness.dark;
    return Styles(
        primary:
            isDarkTheme ? const Color(0xffFEC674) : const Color(0xffFBAC58),
        primary60:
            isDarkTheme ? const Color(0x99fec674) : const Color(0x99fbac58),
        primary40:
            isDarkTheme ? const Color(0x66fec674) : const Color(0x66fbac58),
        surface:
            isDarkTheme ? const Color(0xff16172E) : const Color(0xffFFFFFF),
        onSurface:
            isDarkTheme ? const Color(0xffFFFFFF) : const Color(0xff000000),
        onSurfaceHigh:
            isDarkTheme ? const Color(0xdeffffff) : const Color(0xde000000),
        onSurfaceMedium:
            isDarkTheme ? const Color(0x99ffffff) : const Color(0x99000000),
        onSurfaceDisabled:
            isDarkTheme ? const Color(0x61ffffff) : const Color(0x61000000),
        onSurfaceElevation1dp:
            isDarkTheme ? const Color(0xDFFFFFF) : const Color(0xD000000),
        onSurfaceElevation3dp:
            isDarkTheme ? const Color(0x14FFFFFF) : const Color(0x14000000),
        redColor:
            isDarkTheme ? const Color(0xFFF99F9A) : const Color(0xFFFE5C4D),
        orangeColor:
            isDarkTheme ? const Color(0xFFFEB59C) : const Color(0xFFFE9874),
        greenColor:
            isDarkTheme ? const Color(0xFF9FDFA1) : const Color(0xFF74D378),
        blueColor:
            isDarkTheme ? const Color(0xFF87D7FC) : const Color(0xFF5AC8FA),
        onboardingTitleTextStyle: isDarkTheme
            ? TextStyle(
                fontSize: 18,
                color: const Color(0xffFFFFFF),
                fontWeight: FontWeight.bold)
            : TextStyle(
                fontSize: 18,
                color: const Color(0xff000000),
                fontWeight: FontWeight.bold));
  }
}
