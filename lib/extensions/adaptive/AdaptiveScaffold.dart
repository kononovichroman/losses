import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:losses/extensions/adaptive/AdaptiveRoot.dart';

class AdaptiveScaffold extends StatefulWidget {
  final GlobalKey<ScaffoldState>? scaffoldKey;
  final Widget? title;
  final Widget body;
  final List<Widget>? actions;
  final Widget? appBar;
  final Widget? drawer;
  final Widget? endDrawer;
  final Widget? floatingActionButton;
  final FloatingActionButtonLocation? floatingActionButtonLocation;
  final FloatingActionButtonAnimator? floatingActionButtonAnimator;
  final CupertinoNavigationBar? cupertinoNavigationBar;
  final Widget? bottomNavigationBar;
  final Color? backgroundColor;

  final bool largeCupertino;

  AdaptiveScaffold(
      {this.scaffoldKey,
      this.title,
      required this.body,
      this.actions,
      this.appBar,
      this.cupertinoNavigationBar,
      this.drawer,
      this.endDrawer,
      this.backgroundColor,
      this.floatingActionButton,
      this.floatingActionButtonLocation,
      this.floatingActionButtonAnimator,
      this.bottomNavigationBar})
      : largeCupertino = false;

  AdaptiveScaffold.large(
      {this.scaffoldKey,
      this.title,
      required this.body,
      this.actions,
      this.appBar,
      this.cupertinoNavigationBar,
      this.drawer,
      this.endDrawer,
      this.floatingActionButton,
      this.floatingActionButtonLocation,
      this.floatingActionButtonAnimator,
      this.bottomNavigationBar,
      this.backgroundColor})
      : largeCupertino = true;

  @override
  _AdaptiveScaffoldState createState() => _AdaptiveScaffoldState();
}

class _AdaptiveScaffoldState extends State<AdaptiveScaffold> {
  @override
  Widget build(BuildContext context) {
    AdaptiveRoot _inheritance = AdaptiveRoot.of(context)!;

    //Painting status bar
    SystemChrome.setSystemUIOverlayStyle(
        MediaQuery.of(context).platformBrightness == Brightness.dark
            ? SystemUiOverlayStyle(
                statusBarColor: Colors.transparent,
                systemNavigationBarIconBrightness: Brightness.light,
                statusBarIconBrightness: Brightness.light,
                statusBarBrightness: Brightness.dark,
              )
            : SystemUiOverlayStyle(
                statusBarColor: Colors.transparent,
                systemNavigationBarIconBrightness: Brightness.light,
                statusBarIconBrightness: Brightness.dark,
                statusBarBrightness: Brightness.light,
        ));
    return _inheritance.adaptiveState == AdaptiveState.Material
        ? Scaffold(
            backgroundColor: widget.backgroundColor,
            key: widget.scaffoldKey,
            appBar: widget.title == null && widget.appBar == null
                ? null
                : widget.appBar as PreferredSizeWidget? ??
                    AppBar(
                      title: widget.title,
                      actions: widget.actions,
                    ),
            body: widget.body,
            drawer: widget.drawer,
            endDrawer: widget.endDrawer,
            floatingActionButton: widget.floatingActionButton,
            floatingActionButtonAnimator: widget.floatingActionButtonAnimator,
            floatingActionButtonLocation: widget.floatingActionButtonLocation,
            bottomNavigationBar: widget.bottomNavigationBar,
          )
        : CupertinoPageScaffold(
            backgroundColor: widget.backgroundColor,
            key: widget.scaffoldKey,
            navigationBar:
                widget.title == null && widget.cupertinoNavigationBar == null
                    ? null
                    : widget.cupertinoNavigationBar == null
                        ? widget.largeCupertino
                            ? null
                            : CupertinoNavigationBar(
                                middle: widget.title,
                                trailing: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: widget.actions ?? [],
                                ),
                              )
                        : widget.cupertinoNavigationBar,
            child: widget.largeCupertino
                ? CustomScrollView(
                    slivers: <Widget>[
                      CupertinoSliverNavigationBar(
                        backgroundColor: widget.backgroundColor,
                        largeTitle: widget.title,
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: widget.actions ?? [],
                        ),
                      ),
                      SliverToBoxAdapter(
                        child: widget.body,
                      ),
                    ],
                  )
                : widget.body,
          );
  }
}
