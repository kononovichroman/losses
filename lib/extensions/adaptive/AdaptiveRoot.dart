import 'dart:io';
import 'package:flutter/material.dart';

enum AdaptiveState { Cupertino, Material }

class AdaptiveRoot extends InheritedWidget {
  static AdaptiveState getStateByPlatform() {
    return Platform.isAndroid
        ? AdaptiveState.Material
        : Platform.isIOS
            ? AdaptiveState.Cupertino
            : AdaptiveState.Material;
  }

  const AdaptiveRoot({
    Key? key,
    required this.adaptiveState,
    required Widget child,
  }) : super(key: key, child: child);

  final AdaptiveState adaptiveState;

  static AdaptiveRoot? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<AdaptiveRoot>();
  }

  @override
  bool updateShouldNotify(AdaptiveRoot oldWidget) {
    return adaptiveState != oldWidget.adaptiveState;
  }
}
