class Dimensions{
  static const double onboardingMarginTitle = 52.0;
  static const double onboardingMarginPicker = 90.0;
  static const double onboardingSizePicker = 260.0;
  static const double onboardingDotSize = 8.0;
  static const double onboardingMarginDot = 24.0;
  static const int onboardingDurationChangePage = 300;
}